import axios from "axios";
import {getConfig} from './getConfig'
import {path} from './path'

async function createHelp(PATH,data){
    let result;
    let newData = JSON.parse(JSON.stringify(data))
    let conf = await getConfig()
    newData.status = "NEW"
    await axios.post(path+PATH, newData,conf).then(res => {
        console.log(res); 
        result = true
     }).catch(function (error){
        console.log(error);
        result = false
    })
    return result
}
async function createUser(data){
    console.log(data)
    let result;
    try{
       await axios.post(path+'/api/v1/auth/register', data,await getConfig()).then(res => {
            console.log(res);
            result = true
        });
    } catch(e){
        result = false
        console.log(e)
    }
    return result
}
async function logIn(data){
    try{
        let result;
        await axios.post(path+'/api/v1/auth/login',data).then(res => {
            console.log(res, res.status, res.data)
            if(res.status == 200){
                console.log(true)
                result = res.data
                
            }
        });
        return result
     } catch (e){
         if(e.response.status == 500){
            return {
                msg:'Неверный логин или пароль',
                errType:'log',
            }
        } else {
            return {
                msg:'Что то пошло не так, попробуйте перезагрузить страницу, если не помогло - обратитесь к администратору',
                errType:'serv',
            }
        }
     }
 
}

export {createUser,logIn, createHelp}