import axios from "axios";
import {getConfig} from './getConfig'
import {path} from './path'

async function updateUser(data){
    let result;
    let conf = await getConfig()
    console.log(data)
    await axios.put(path+'/api/v1/auth/'+data.id, data,conf).then(res => {
        console.log(res); 
        result = true
     }).catch(function (error){
        console.log(error);
        result = false
    })
    return result
}

export {updateUser}