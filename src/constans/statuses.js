const statuses = {
    NEW:{
        trans:'Не в работе',
        color:'blue',
    } ,
    IN_WORK:{
        trans:'В работе',
        color:'yellow',
    } ,
    CANCEL:{
        trans:'Отмена',
        color:'gray',
    } ,
    DONE:{
        trans:'Выполнено',
        color:'green',
    } ,
    NOT_SOLVED:{
        trans:'Не решено',
        color:'red',
    } ,
    NOT_ACCEPTED:{
        trans:'Не принято',
        color:'violet',
    },
    
}
export { statuses }