import { getToken } from '@/server/getConfig'
import {getAuthUser} from '@/server/get'
async function createdRefresh(){
    try{
        const token = await getToken()
        if(token){
            await getAuthUser(token)
        }
    } catch {
        console.log(e)
    }
}
export {createdRefresh}